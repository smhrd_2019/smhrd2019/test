package JDBC;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class PhoneDAO {
	Connection conn = null;
	PreparedStatement pst = null;
	ArrayList<PhoneDTO> list = new ArrayList<>();

	public PhoneDAO() {
		try {

			String url = "jdbc:oracle:thin:@127.0.0.1:1521:xe";
			String id = "hr";
			String pw = "hr";

			Class.forName("oracle.jdbc.driver.OracleDriver");
			// 2. JDBC에 맞춰서 DBMS회사에서 만들어논 클래스파일을 동적로딩
			// Java는 우리가 어떤 DBMS를 사용할지몰라
			conn = DriverManager.getConnection(url, id, pw);
			// 3. DBMS로 가서 사용자의 정보를 인증을 받고 DB를 핸들링 할 수 있는 Connection리턴

			if (conn != null) {
				System.out.println("실행");
			} // 확인

		} catch (Exception e) {

		}
	}

	public ArrayList<PhoneDTO> searchAll() {

		String sql = "select * from phone";
		try {
			pst = conn.prepareStatement(sql);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {
				String name = rs.getString(1);
				int age = rs.getInt(2);
				String tel = rs.getString(3);
				
				PhoneDTO dto = new PhoneDTO(name, age, tel);
				
				list.add(dto);
			}
			
			return list;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}
	
	
	public int addNum(String name, int age, String tel){
		String sql = "insert into phone values(?,?,?)";
		try {
			pst = conn.prepareStatement(sql);
			pst.setString(1, name);
			pst.setInt(2, age);
			pst.setString(3, tel);
			
			int cnt = pst.executeUpdate();
			
			return cnt;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

	public PhoneDTO searchOne(int index){
		String sql = "select *from phone where name = ?";
		
		try {
			pst = conn.prepareStatement(sql);
			String name = list.get(index).getName();
			pst.setString(1, name);
			ResultSet rs = pst.executeQuery();
			
			return list.get(index);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
}
