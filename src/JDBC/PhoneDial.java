package JDBC;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

public class PhoneDial extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JPanel panel_1;
	private JTextField textField;
	private JLabel label_1;
	private JTextField textField_1;
	private JTextField textField_2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			PhoneDial dialog = new PhoneDial();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public PhoneDial() {
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		SpringLayout sl_contentPanel = new SpringLayout();
		contentPanel.setLayout(sl_contentPanel);
		{
			panel_1 = new JPanel();
			panel_1.setBackground(new Color(173, 216, 230));
			sl_contentPanel.putConstraint(SpringLayout.NORTH, panel_1, 0, SpringLayout.NORTH, contentPanel);
			sl_contentPanel.putConstraint(SpringLayout.WEST, panel_1, 0, SpringLayout.WEST, contentPanel);
			sl_contentPanel.putConstraint(SpringLayout.SOUTH, panel_1, 45, SpringLayout.NORTH, contentPanel);
			sl_contentPanel.putConstraint(SpringLayout.EAST, panel_1, 412, SpringLayout.WEST, contentPanel);
			contentPanel.add(panel_1);
		}
		{
			JPanel panel = new JPanel();
			sl_contentPanel.putConstraint(SpringLayout.NORTH, panel, 2, SpringLayout.SOUTH, panel_1);
			sl_contentPanel.putConstraint(SpringLayout.WEST, panel, 0, SpringLayout.WEST, contentPanel);
			sl_contentPanel.putConstraint(SpringLayout.SOUTH, panel, 134, SpringLayout.SOUTH, panel_1);
			panel_1.setLayout(new CardLayout(0, 0));
			{
				JLabel lblNewLabel = new JLabel("\uC804\uD654\uBC88\uD638 \uB4F1\uB85D");
				lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
				lblNewLabel.setFont(new Font("a아메리카노B", Font.PLAIN, 20));
				panel_1.add(lblNewLabel, "name_103561455729531");
			}
			sl_contentPanel.putConstraint(SpringLayout.EAST, panel, 412, SpringLayout.WEST, contentPanel);
			contentPanel.add(panel);
			panel.setLayout(new GridLayout(1, 0, 0, 0));
			{
				JPanel panel_3 = new JPanel();
				panel_3.setBackground(new Color(173, 255, 47));
				panel.add(panel_3);
				SpringLayout sl_panel_3 = new SpringLayout();
				panel_3.setLayout(sl_panel_3);
				{
					label_1 = new JLabel("\uC774\uB984");
					sl_panel_3.putConstraint(SpringLayout.NORTH, label_1, 5, SpringLayout.NORTH, panel_3);
					sl_panel_3.putConstraint(SpringLayout.WEST, label_1, 72, SpringLayout.WEST, panel_3);
					panel_3.add(label_1);
				}
				{
					JLabel lblNewLabel_1 = new JLabel("\uB098\uC774");
					sl_panel_3.putConstraint(SpringLayout.NORTH, lblNewLabel_1, 29, SpringLayout.SOUTH, label_1);
					sl_panel_3.putConstraint(SpringLayout.EAST, lblNewLabel_1, 0, SpringLayout.EAST, label_1);
					panel_3.add(lblNewLabel_1);
				}
				{
					JLabel lblNewLabel_2 = new JLabel("\uC804\uD654\uBC88\uD638");
					sl_panel_3.putConstraint(SpringLayout.WEST, lblNewLabel_2, 0, SpringLayout.WEST, label_1);
					sl_panel_3.putConstraint(SpringLayout.SOUTH, lblNewLabel_2, -10, SpringLayout.SOUTH, panel_3);
					panel_3.add(lblNewLabel_2);
				}
			}
			{
				JPanel panel_2 = new JPanel();
				panel_2.setBackground(new Color(255, 140, 0));
				panel.add(panel_2);
				SpringLayout sl_panel_2 = new SpringLayout();
				panel_2.setLayout(sl_panel_2);
				{
					textField = new JTextField();
					sl_panel_2.putConstraint(SpringLayout.NORTH, textField, 10, SpringLayout.NORTH, panel_2);
					sl_panel_2.putConstraint(SpringLayout.EAST, textField, -41, SpringLayout.EAST, panel_2);
					panel_2.add(textField);
					textField.setColumns(10);
				}
				{
					textField_1 = new JTextField();
					sl_panel_2.putConstraint(SpringLayout.WEST, textField_1, 0, SpringLayout.WEST, textField);
					sl_panel_2.putConstraint(SpringLayout.SOUTH, textField_1, -10, SpringLayout.SOUTH, panel_2);
					panel_2.add(textField_1);
					textField_1.setColumns(10);
				}
				{
					textField_2 = new JTextField();
					sl_panel_2.putConstraint(SpringLayout.SOUTH, textField_2, -19, SpringLayout.NORTH, textField_1);
					sl_panel_2.putConstraint(SpringLayout.EAST, textField_2, 0, SpringLayout.EAST, textField);
					panel_2.add(textField_2);
					textField_2.setColumns(10);
				}
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						PhoneDAO dao = new PhoneDAO();
						int cnt = dao.addNum(textField.getText(), Integer.parseInt(textField_1.getText()), textField_2.getText());
						
						if(cnt>0){
							System.out.println("완료");
						}
						setVisible(false);
						PhoneGUI gui = new PhoneGUI();
						gui.main(null);
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}

}
