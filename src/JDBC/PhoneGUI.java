package JDBC;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.SpringLayout;
import javax.swing.JPanel;
import java.awt.CardLayout;
import javax.swing.JLabel;
import javax.swing.JList;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import java.awt.Font;
import java.awt.Color;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class PhoneGUI {

	private JFrame frame;
	static DefaultListModel model;
	PhoneDAO dao = new PhoneDAO();
	PhoneDial dialog;
	static PhoneGUI window;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					window = new PhoneGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public PhoneGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 384);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		SpringLayout springLayout = new SpringLayout();
		frame.getContentPane().setLayout(springLayout);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.GRAY);
		springLayout.putConstraint(SpringLayout.NORTH, panel, 10, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, panel, 10, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, panel, 66, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, panel, 422, SpringLayout.WEST, frame.getContentPane());
		frame.getContentPane().add(panel);
		
		JPanel panel_1 = new JPanel();
		springLayout.putConstraint(SpringLayout.NORTH, panel_1, 6, SpringLayout.SOUTH, panel);
		springLayout.putConstraint(SpringLayout.WEST, panel_1, 10, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, panel_1, 177, SpringLayout.SOUTH, panel);
		panel.setLayout(new CardLayout(0, 0));
		
		JLabel lbl_list = new JLabel("\uC804\uD654\uBAA9\uB85D");
		lbl_list.setFont(new Font("a아메리카노B", Font.BOLD, 20));
		lbl_list.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(lbl_list, "name_102024665188979");
		springLayout.putConstraint(SpringLayout.EAST, panel_1, 422, SpringLayout.WEST, frame.getContentPane());
		frame.getContentPane().add(panel_1);
		panel_1.setLayout(new GridLayout(1, 0, 0, 0));
		
		JPanel panel_2 = new JPanel();
		panel_1.add(panel_2);
		panel_2.setLayout(new GridLayout(1, 0, 0, 0));
		
		// JLsit 데이터 출력 시 
		// DefaultListModel
		
		model = new DefaultListModel<>();
		ArrayList<PhoneDTO> alist = new ArrayList<>();
		alist = dao.searchAll();
		//System.out.println(alist.get(0).getName());
		for(int i = 0; i<alist.size(); i++){
			model.addElement(alist.get(i).getName());
		}
		
		JList list = new JList(model);
		panel_2.add(list);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBackground(new Color(102, 205, 170));
		panel_1.add(panel_3);
		SpringLayout sl_panel_3 = new SpringLayout();
		panel_3.setLayout(sl_panel_3);
		
		JLabel lbl_name = new JLabel("\uC774\uB984");
		sl_panel_3.putConstraint(SpringLayout.NORTH, lbl_name, 26, SpringLayout.NORTH, panel_3);
		sl_panel_3.putConstraint(SpringLayout.WEST, lbl_name, 23, SpringLayout.WEST, panel_3);
		panel_3.add(lbl_name);
		
		JLabel lbl_bar1 = new JLabel("-");
		sl_panel_3.putConstraint(SpringLayout.NORTH, lbl_bar1, 26, SpringLayout.NORTH, panel_3);
		panel_3.add(lbl_bar1);
		
		JLabel lbl_age = new JLabel("\uB098\uC774");
		sl_panel_3.putConstraint(SpringLayout.WEST, lbl_age, 0, SpringLayout.WEST, lbl_name);
		panel_3.add(lbl_age);
		
		JLabel lbl_bar2 = new JLabel("-");
		sl_panel_3.putConstraint(SpringLayout.WEST, lbl_bar1, 0, SpringLayout.WEST, lbl_bar2);
		sl_panel_3.putConstraint(SpringLayout.EAST, lbl_bar1, 0, SpringLayout.EAST, lbl_bar2);
		panel_3.add(lbl_bar2);
		
		JLabel lbl_tel = new JLabel("\uC804\uD654\uBC88\uD638");
		sl_panel_3.putConstraint(SpringLayout.SOUTH, lbl_age, -34, SpringLayout.NORTH, lbl_tel);
		sl_panel_3.putConstraint(SpringLayout.WEST, lbl_tel, 0, SpringLayout.WEST, lbl_name);
		sl_panel_3.putConstraint(SpringLayout.SOUTH, lbl_tel, -21, SpringLayout.SOUTH, panel_3);
		panel_3.add(lbl_tel);
		
		JLabel lbl_bar3 = new JLabel("-");
		sl_panel_3.putConstraint(SpringLayout.WEST, lbl_bar3, 33, SpringLayout.EAST, lbl_tel);
		sl_panel_3.putConstraint(SpringLayout.EAST, lbl_bar3, -10, SpringLayout.EAST, panel_3);
		sl_panel_3.putConstraint(SpringLayout.WEST, lbl_bar2, 0, SpringLayout.WEST, lbl_bar3);
		sl_panel_3.putConstraint(SpringLayout.SOUTH, lbl_bar2, -34, SpringLayout.NORTH, lbl_bar3);
		sl_panel_3.putConstraint(SpringLayout.EAST, lbl_bar2, 0, SpringLayout.EAST, lbl_bar3);
		sl_panel_3.putConstraint(SpringLayout.NORTH, lbl_bar3, 0, SpringLayout.NORTH, lbl_tel);
		panel_3.add(lbl_bar3);
		
		JPanel panel_4 = new JPanel();
		panel_4.setBackground(Color.ORANGE);
		springLayout.putConstraint(SpringLayout.NORTH, panel_4, 6, SpringLayout.SOUTH, panel_1);
		springLayout.putConstraint(SpringLayout.WEST, panel_4, 10, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, panel_4, 84, SpringLayout.SOUTH, panel_1);
		springLayout.putConstraint(SpringLayout.EAST, panel_4, 422, SpringLayout.WEST, frame.getContentPane());
		frame.getContentPane().add(panel_4);
		SpringLayout sl_panel_4 = new SpringLayout();
		panel_4.setLayout(sl_panel_4);
		
		JButton btn_add = new JButton("\uC5F0\uB77D\uCC98\uCD94\uAC00");
		btn_add.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				window.frame.setVisible(false);
				//window.frame.setVisible(false);
				dialog = new PhoneDial();
				dialog.setVisible(true);
				
				
			}
		});
		sl_panel_4.putConstraint(SpringLayout.NORTH, btn_add, 23, SpringLayout.NORTH, panel_4);
		sl_panel_4.putConstraint(SpringLayout.WEST, btn_add, 51, SpringLayout.WEST, panel_4);
		panel_4.add(btn_add);
		
		JButton btn_del = new JButton("\uC5F0\uB77D\uCC98\uC0AD\uC81C");
		sl_panel_4.putConstraint(SpringLayout.NORTH, btn_del, 0, SpringLayout.NORTH, btn_add);
		sl_panel_4.putConstraint(SpringLayout.EAST, btn_del, -34, SpringLayout.EAST, panel_4);
		panel_4.add(btn_del);
		
		list.addListSelectionListener(new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if(e.getValueIsAdjusting()){
					int index = list.getSelectedIndex();
					PhoneDTO dto =dao.searchOne(index);
					lbl_bar1.setText(dto.getName());
					lbl_bar2.setText(Integer.toString(dto.getAge()));
					lbl_bar3.setText(dto.getTel());
				}
				
			}
		});
	}
}
